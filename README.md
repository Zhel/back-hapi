
# Documentation :
## BACK-END HAPI JS


```
http://localhost:3000/{route}

Instructions:
cd back-hapi
npm install
npm start
```
----
[I/MESSAGE](#I/MESSAGE)
1. GET  /ALLMessages
2. GET  /message/{id}
3. POST /message
4. PUT  /message
5. DELETE /message/{id}





----
## I/MESSAGE
```
/message
```
```json
GET /AllMessages

'Pas de paramètres'

Résultat:

{
    "table": [
        {
            "id": 93,
            "message": "salut"
        },
        {
            "id": 631,
            "message": "serpent"
        },
        {
            "id": 731,
            "message": "non"
        },
        {
            "id": 339,
            "message": "u4"
        },
        {
            "id": 144,
            "message": "bon"
        },
        {
            "id": 463,
            "message": "demain"
        },
        {
            "id": 982,
            "message": "oui"
        }
    ]
}

```
```json
GET /message/{id}

Paramètre dans l'url, {id}

Résultat:

'Posted ! {"id":948,"message":"ko"}'

```

```json
POST /message

JSON(application/json)

{
    "message": "{message}"
}

Résultat:

'Posted ! {"id":948,"message":"ko"}'

```

```json
PUT /message

JSON(application/json)

{
    "id": "{id}",
    "message": "{message}"
}

Résultat:

'Edited !'
or
'Wrong ID !'

```
```json
DELETE /message/{id}

Paramètre dans l'url, {id}

Résultat:

'Deleted !'
or
'Echec !'

```

