'use strict';

const fs = require('fs')

var obji = {}
obji.table = []
var log = {}
log.table = []

fs.readFile('db.json', (err, data) => {
    if (err) throw err;
    //console.log(JSON.parse(data));
  });


const Hapi = require('@hapi/hapi');

const init = async () => {

    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });

    function writeLog(route, request, params, state) {

        const xFF = request.headers['x-forwarded-for']
        const ip = xFF ? xFF.split(',')[0] : request.info.remoteAddress

        let db = require('./ServerLog.json')
        if (JSON.stringify(db) === '{}') {
            fs.writeFileSync('ServerLog.json', JSON.stringify(log), (err) => {
                if (err) throw err;
                console.log('The file has been saved!');
              });
        }

        fs.readFile('ServerLog.json', 'utf8', function readFileCallback(err, data){
            if (err){
                console.log(err);
            } else {
                log = JSON.parse(data);
                log.table.push({id: (Number(Object.keys(log.table).length)+1), date: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''), 
                ip: ip, route: route, params: params, state: state});

            var json = JSON.stringify(log);
            fs.writeFile('ServerLog.json', json, 'utf8', (err) => {
                if (err) throw err;
                console.log('The file has been saved!');
              }); 
        }});
    }

    server.route({
        method: 'GET',
        path: '/AllMessages',
        handler: (request, h) => {

            let db = require('./db.json')
            console.log(db)

            writeLog("GET /ALLMessages", request, "aucun", "OK")
            return db;
        }
    });


    server.route({
        method: 'GET',
        path: '/message/{id}',
        handler:  (request, h) => {

            var realID = request.params.id
            var index = 1001;
            var messageAtId = ""

            var data = fs.readFileSync('db.json', 'utf8')
            obji = JSON.parse(data); 
                    for (var idi = 0; idi < Object.keys(obji.table).length; idi++) {
                        if (Number(realID) === obji.table[idi].id) {
                            index = idi
                        }
                    }
            if (index !== 1001) {
                messageAtId = obji.table[index].message;
                writeLog("GET /message/{id}", request, realID, "OK")
                return "Message at id : " + realID + " is : " + messageAtId
            }
            if (index === 1001) {
                writeLog("GET /message/{id}", request, realID, "erreur")
                return "Wrong ID !"
            }
        }
    });

    server.route({
        method: 'POST',
        path: '/message',
        handler: (request, h) => {

            let db = require('./db.json')
            var newMessage = request.payload.message
            var newID = 0
            if (JSON.stringify(db) === '{}') {
                fs.writeFileSync('db.json', JSON.stringify(obji), (err) => {
                    if (err) throw err;
                    console.log('The file has been saved!');
                  });
            }
            
            var data = fs.readFileSync('db.json', 'utf8', function readFileCallback(err, data){
                if (err){ 
                    console.log(err);
                } else {       
            }});

            obji = JSON.parse(data);
            newID = Math.floor(Math.random() * Math.floor(1000))
            obji.table.push({id: newID, message: newMessage});
            var json = JSON.stringify(obji);
            fs.writeFile('db.json', json, 'utf8', (err) => {
                if (err) throw err;
                console.log('The file has been saved!');
            });
            writeLog("POST /message", request, {id: newID, message: newMessage}, "OK")
        return "Posted ! " + JSON.stringify({id: newID, message: newMessage})
        }
    });

    server.route({
        method: 'PUT',
        path: '/message',
        handler: (request, h) => {

            var realID = request.payload.id
            var newMessage = request.payload.message
            var index = 1001;

           var data = fs.readFileSync('db.json', 'utf8', function readFileCallback(err, data){
                if (err){
                    console.log(err);
                } else {
            }});

            obji = JSON.parse(data);
            for (var idi = 0; idi < Object.keys(obji.table).length; idi++) {
                if (realID === obji.table[idi].id) {
                    index = idi
                }
            }
            if (index !== 1001) {
                obji.table[index].message = newMessage;
            }
            var json = JSON.stringify(obji);
         fs.writeFile('db.json', json, 'utf8', (err) => {
            if (err) throw err;
            console.log('The file has been saved!');
          }); 
            if (index !== 1001) {
                writeLog("PUT /message", request, {id: realID, message: newMessage}, "OK")
                return "Edited !"
            }
            if (index === 1001) {
                writeLog("PUT /message", request, "aucun", "erreur")
                return "Wrong ID !"
            }
        }
    });

    server.route({
        method: 'DELETE',
        path: '/message/{id}',
        handler: (request, h) => {

            var realID = request.params.id
            var index = 1001;

            var data = fs.readFileSync('db.json', 'utf8', function readFileCallback(err, data){
                if (err){
                    console.log(err);
                } else {     
            }});

            obji = JSON.parse(data);
            for (var idi = 0; idi < Object.keys(obji.table).length; idi++) {
                if (Number(realID) === obji.table[idi].id) {
                        index = idi
                }
            }
            if (index !== 1001) {
                obji.table.splice(index, 1)
            } 
            var json = JSON.stringify(obji);
            fs.writeFile('db.json', json, 'utf8', (err) => {
                if (err) throw err;
                console.log('The file has been saved!');
            }); 

            if (index !== 1001) {
                writeLog("DELETE /message/{id}", request, realID, "OK")
                return "Deleted !"
            }
            if (index === 1001) {
                writeLog("DELETE /message/{id}", request, realID, "erreur")
                return "Echec !"
            }
        }
    });

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();